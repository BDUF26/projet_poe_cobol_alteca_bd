000001       ******************************************************************
000002       * DCLGEN TABLE(IBMUSER.TBOPE)                                    *
000003       *        LIBRARY(IBMUSER.COB.DCLGEN(DCOPE))                      *
000004       *        LANGUAGE(COBOL)                                         *
000005       *        NAMES(HO-)                                              *
000006       *        QUOTE                                                   *
000007       *        COLSUFFIX(YES)                                          *
000008       * ... IS THE DCLGEN COMMAND THAT MADE THE FOLLOWING STATEMENTS   *
000009       ******************************************************************
000010            EXEC SQL DECLARE IBMUSER.TBOPE TABLE
000011            ( COPOPE                         CHAR(3),
000012              LIBOPE                         CHAR(20),
000013              MNTMIN                         DECIMAL(9, 2),
000014              MNTMAX                         DECIMAL(9, 2)
000015            ) END-EXEC.
000016       ******************************************************************
000017       * COBOL DECLARATION FOR TABLE IBMUSER.TBOPE                      *
000018       ******************************************************************
000019        01  DCLTBOPE.
000020       *                       COPOPE
000021            10 HO-COPOPE            PIC X(3).
000022       *                       LIBOPE
000023            10 HO-LIBOPE            PIC X(20).
000024       *                       MNTMIN
000025            10 HO-MNTMIN            PIC S9(7)V9(2) USAGE COMP-3.
000026       *                       MNTMAX
000027            10 HO-MNTMAX            PIC S9(7)V9(2) USAGE COMP-3.
000028       ******************************************************************
000029       * THE NUMBER OF COLUMNS DESCRIBED BY THIS DECLARATION IS 4       *
000030       ******************************************************************